﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unity.Services.CloudCode.Core;

namespace ChessCloudCode
{
    public interface IRandomNumber
    {
        public int GetRandomNumber();
    }

    public class LockedRandomNumber : IRandomNumber
    {
        private int randomNumber;

        public LockedRandomNumber()
        {
            randomNumber = Random.Shared.Next();
        }

        public int GetRandomNumber()
        {
            return randomNumber;
        }
    }

}
