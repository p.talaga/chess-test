using ChessCloudCode;
using NUnit.Framework;
using Microsoft.Extensions.Logging;
using Unity.Services.CloudCode.Apis;
using Moq;
using Chess;

namespace Tests2
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        //_should_<expectation>_when_<condition>

        [Test]
        public void Starting_color_should_be_white()
        {
            ChessBoard chessBoard = ChessBoard.LoadFromFen("rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1");

            var x = chessBoard.Turn == PieceColor.White;

            Assert.That(x, Is.True);
        }

        [Test]
        public void Color_should_be_black_when_white_moves()
        {
            ChessBoard chessBoard = ChessBoard.LoadFromFen("rnbqkbnr/pppppppp/8/8/4P3/8/PPPP1PPP/RNBQKBNR b KQkq e3 0 1");

            var x = chessBoard.Turn == PieceColor.Black;

            Assert.That(x, Is.True);
        }

        [Test]
        public void abc()
        {
            var x = new Mock<IRandomNumber>();
            x.Setup(x => x.GetRandomNumber()).Returns(1);

            var y = new BasedOnRandomNumber(x.Object);

            Assert.That(y.RandomNumber.GetRandomNumber(), Is.EqualTo(1));   
        }


        public class RandomNumber : IRandomNumber
        {
            Random _rnd = new();

            public int GetRandomNumber()
            {
                return _rnd.Next();
            }
        }

        public class BasedOnRandomNumber
        {
            public IRandomNumber RandomNumber;

            public BasedOnRandomNumber(IRandomNumber randomNumber)
            {
                RandomNumber = randomNumber;    
            }
        }

    }
}